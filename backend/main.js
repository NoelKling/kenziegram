// ESM syntax and skeleton from vince demo
import express from "express";
import cors from "cors";
import { uploader, UPLOAD_DIRECTORY, getUploadedFiles, findUploadedFile } from "./utils";

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});
app.use("/", express.static(UPLOAD_DIRECTORY));

app.use("/upload", express.static(UPLOAD_DIRECTORY));

app.get("/photos", async (req, res) => {
  try {
    const files = await getUploadedFiles();
    res.json(files.map((file) => `/upload/${file}`));
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

app.get("/photos/:photoId", async (req, res) => {
  try {
    const { size } = await findUploadedFile(req.params.photoId);
    res.json({ src: req.params.photoId, size: `${size}` });
  } catch (err) {
    res.status(404).json({ message: "file not found" });
  }
});

app.post("/upload", uploader.single("photo"), function (req, res) {
  res.json({
    photo: req.file.path,
  });
});

app.get("*", (req, res) => {
  res.status(404).send("Error not found");
});

app.listen(5000, () => {
  console.log("Express server is now running on port 5000");
});

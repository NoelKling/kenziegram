import axios from "axios";

export const API_BASE_URL = "http://localhost:5000";

const axiosInstance = axios.create({
  baseURL: API_BASE_URL,
});

export default axiosInstance;

export const fetchPhotos = async () => {
  const { data } = await axiosInstance.get("/photos");
  return data;
};

export const fetchSinglePhoto = async (pic) => {
  const photo = await axiosInstance.get(`/photos/${pic}`);
  console.log(photo.data.size);
  return photo.data.size;
};

export const uploadPhoto = async (data) => {
  const formData = new FormData();
  formData.append("photo", data);
  await axiosInstance.post("/upload", formData);
};

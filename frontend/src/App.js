import React from "react";
import { Switch, Route } from "react-router-dom";
import Navigation from "./components/nav/Nav";
import Home from "./components/home/Home";
import Photos from "./components/photos/Photos";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Navigation />
      <nav>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/photos">
            <Photos />
          </Route>
        </Switch>
      </nav>
    </div>
  );
}

export default App;

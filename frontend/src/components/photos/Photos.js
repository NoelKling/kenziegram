import React, { useEffect, useState, createRef } from "react";
import { fetchPhotos, API_BASE_URL, fetchSinglePhoto } from "../../api.js";
import Modal from "react-bootstrap/Modal";
import Card from "react-bootstrap/Card";

function Photos() {
  const [photos, setPhotos] = useState([]);
  const [image, setImage] = useState({ fileSize: null, photo: null });
  const [show, setShow] = useState(false);

  useEffect(() => {
    async function getPhotos() {
      setPhotos(await fetchPhotos());
    }

    getPhotos();
  }, []);

  const handleShow = async (pic) => {
    const splitPic = pic.alt.split("/");
    async function getSinglePhoto() {
      return await fetchSinglePhoto(splitPic[2]).then((result) => {
        return result;
      });
    }

    const fileSize = await getSinglePhoto();
    setImage({
      fileSize: fileConversion(fileSize),
      photo: pic.src,
    });

    setShow(true);
  };

  const handleClose = () => setShow(false);

  const fileConversion = (file) => {
    if (file < 1000) {
      return `${file} B`;
    }
    let size = ["B", "KB", "MB"];

    let byteCon = Math.floor(Math.log(file) / Math.log(1000));

    if (byteCon === 0) return byteCon + " " + size[byteCon];

    return (file / Math.pow(1000, byteCon)).toFixed(1) + " " + size[byteCon];
  };

  const handleModalCard = () => {
    return (
      <Modal show={show} ref={createRef()} className={"modalComp"} onClick={handleClose}>
        <Modal.Body className="model">
          <Card>
            <Card.Img variant="top" src={image.photo} />
            <Card.Body>
              <Card.Title>{image.fileSize}</Card.Title>
            </Card.Body>
          </Card>
        </Modal.Body>
      </Modal>
    );
  };

  return (
    <div className="App">
      <div className="photos">
        {photos.map((pic) => (
          <img key={pic} src={`${API_BASE_URL}${pic}`} alt={pic} onClick={(pic) => handleShow(pic.target)} />
        ))}
        {show ? handleModalCard() : null}
      </div>
    </div>
  );
}

export default Photos;
